#!/bin/bash

# An automated script that updates JAR files.
# Supports Spigot - BungeeCord - Paper - Waterfall
# Required Permissions: sudo chmod +rwx $0

# By Crypto Morin - v3.0.0
# GitLab: https://gitlab.com/CryptoMorin/papi
# Under MIT License: https://gitlab.com/CryptoMorin/papi/blob/master/LICENSE
# Paper Endpoint Wiki: https://paper.readthedocs.io/en/stable/site/api.html

#----------------------------------------------------------------------
# Default Implementations
#----------------------------------------------------------------------
declare -r PAPI_VER='3.0.0'

# Prints a stderr message with red color.
err() {
	echo -e "\033[0;31m$@\033[0m" 1>&2
	echo "[$(date +%T)] [ERROR] $@" >> "Warnings.log"
}

# Prints a normal debug message with aqua color.
debug() {
	if get_boolean Debug; then
		echo -e "\033[2;96m[$(date +%T)] $@\033[0m"
		echo "[$(date +%T)] $@" >> "Debug.log"
	fi
}

# Prints a normal warnings with yellow color.
warn() {
	echo -e "\033[0;33m$@\033[0m"
	echo "[$(date +%T)] [WARN] $@" >> "Warnings.log"
}

# Checks if internet connection is available.
check_connection() {
	echo "Checking internet connection..."

	# Pinging NASA will have compatibility support for Mars internet connections for years later ;)
	if ! wget -q --spider http://www.nasa.gov/; then
		err "No internet connection! Please try again."
		exit 1
	fi
}

# @param path - The path to get the directory. Can be file path.
validate_output() {
	output_dir=$(readlink -m "$1")
	if [[ "$(basename $1)" = *.* ]] || [[ "$(basename $1)" = *"%def_name%"* ]]; then
		output_dir=$(dirname "$output_dir")
	fi

	if ! [ -d "$output_dir" ]; then
		err "Output directory does not exist: $output_dir"
		exit 1
	fi
	if ! [ -w "$output_dir" ]; then
		err "No permission to write in directory: $output_dir"
		exit 1
	fi
}

# Gets index of a string in a string.
# @param one: The original string.
# @param two: The string to get the index of in the original string.
index_of() {
  	x="${1%%$2*}"
  	[[ "$x" = "$1" ]] && echo -1 || echo "${#x}"
}

# POSIX compatible toLowercase function.
to_lower_case() {
	echo "$@" | tr "[:upper:]" "[:lower:]"
}

equals_ignore_case() {
	[ "$(to_lower_case $1)" = "$(to_lower_case $2)" ] && return 0 || return 1
}

# Equals ignorecase any string.
# @param str - The original string to check.
# @param others... - The other strings to compare to the original string.
equals_any() {
	for args in "${@:2}"; do
		equals_ignore_case "$1" "$args" && return 0
	done

	return 1
}

# Supports positive and negative signs.
is_number() {
	[[ $1 =~ ^\+?-?[0-9]+$ ]] && return 0 || return 1
}

# Removes whitespace and line breaks.
trim() {
	trimmed="${1%%[[:space:]]}"
	trimmed="${trimmed##[[:space:]]}"
	echo "$trimmed"
	unset -v trimmed
}

max() {
	if ! is_number $1; then
		err "Invalid numeric value for: $1"
		return 1
	fi
	if ! is_number $2; then
		err "Invalid numeric value for: $2"
		return 1
	fi

	max=$(( $1 > $2 ? $1 : $2 ))
}

# Finds a Java file that the name contains the specified strings.
# @param path - The directory path to search in.
# @param 
find_jar() {
	cmd="find ./$1 -type f -iname "*.jar" -print "
	for args in "${@:3}"; do cmd="$cmd -iname "$args""; done
	echo $(eval $cmd)
}

min() {
	if ! is_number $1; then
		err "Invalid numeric value for: $1"
		return 1
	fi
	if ! is_number $2; then
		err "Invalid numeric value for: $2"
		return 1
	fi

	min=$(( $1 < $2 ? $1 : $2 ))
}

# Windows-like batch PAUSE command with messaging.
pause() {
	if [ -z "$1" ]; then msg="Press any key to continue..."; else msg="$@"; fi
	read -resn 1 -p "$msg"
}

# Windows-like batch choice command with messaging.
choice() {
	[ -z "$1" ] && msg="Press y for yes and n for no" || msg="$@"
	read -resn 1 -p "$msg" response
	[ $response = "y" ] && return 0 || return 1
}

err_exit() {
	echo ""
	for errs in "$@"; do err "$errs"; done
	err "Exiting the program..."
	echo ""
	exit 1
}

# @param line - A line in a YAML file.
declare -A yaml_keys
parse_yaml() {
	if [[ ! "$1" = *:* ]]; then
		err "Invalid YAML option: '$1'"
		return 1
	fi

	option_index=$(index_of "$1" ":")
	option=${1:0:option_index}

	value=${1:option_index+1}
	value=$(trim "$value")
	if [[ ! "$value" = \"* ]] && [[ ! "$value" = \[* ]]; then value=$(to_lower_case "$value"); fi
	value=${value//\"/}
}

#----------------------------------------------------------------------
# Config Handlers
#----------------------------------------------------------------------
declare -A options

# A YAML based parser.
parse_option() {
	if ! parse_yaml "$1"; then return 1; fi
	options[$option]="$value"
	echo "	$option: $value"
}

write_config() {
	for arg in "$@"; do echo "$arg" >> "$config_file"; done
}

# The default config that will be generated if not found.
save_config() {
	echo Generating a new config...
	touch "$config_file"

	write_config "# Note that you need to run the script again if you changed any option." \
	"# WARNING: This is only a similar YAML format. It is not YAML. So it doesn't necessarily work exactly like YAML." \
	"Debug: ${options[Debug]}" "" \
	"# Which of the following projects should be auto-updated?" \
	"Paper: ${options[Paper]}" "Waterfall: ${options[Waterfall]}" "" \
	"# Vanilla, CraftBukkit and Spigot use BuildTools to get the minecraft JAR." \
	"Vanilla: ${options[Vanilla]}" "CraftBukkit: ${options[CraftBukkit]}" "Spigot: ${options[Spigot]}" "BungeeCord: ${options[BungeeCord]}" "" \
	"# The delay between each update." \
	"# Note that 1 day is recommended. Lower values are pointless." \
	"# Spigot usually updates every day, but if you're using Paper you should" \
	"# change this to 3 days." \
	"# Suffixes: 'h' = hour, 'd' = day" \
	"Interval: ${options[Interval]}" "" \
	"# Archive Mode: This option is only useful if you're going to keep all released versions" \
	"# and keep them for undirect purposes. Such as having Multicraft or Pterodactyl panel." \
	"# So this should not be used if you don't have a panel that can have multiple JARs." \
	"# This option won't keep every build, but will keep every release." \
	"# E.g. 1.14.3, 1.14.2, 1.14.1 and etc. are all going to be archived. But not 1.14.1-b123, 1.14.1-b124 and etc." \
	"# This will not work for BungeeCord or Waterfall, you have to keep them updated to the latest version just like how they were designed to be." \
	"# It will also check for 'oldest' and 'newest' options and download/delete missing/extra versions. (read below)" \
	"Archive: ${options[Archive]}" "" \
	"# These two options only works for Paper, Spigot, CraftBukkit and Vanilla JARs." \
	"# E.g. 1.14 will cover 1.14.1, 1.14.2 and etc. 1.14.1 will cover 1.14.1.1, 1.14.1.2 and etc. (It's usually only 3 numbers)" "" \
	"# This option only works if 'archive' mode is enabled. For non-archive, new major versions are not downloaded." \
	"# The newest allowed version to download." \
	"# Works for both archive and non-archive modes. Also works for versions that are not released yet." \
	"# '-1' means no restriction." \
	"Newest: ${options[Newest]}" "" \
	"# This option only works if 'archive' mode is enabled." \
	"# The oldest allowed version to download. Any verions older than this will be ignored and deleted." \
	"# The oldest that is currently available is 1.8.8 (checked 7/24/2019)" \
	"Oldest: ${options[Oldest]}" "" \
	"# This option only works if 'archive' mode is enabled." \
	"# Should only the latest minor version of a major version archived?" \
	"# Understanding that might be a little confusing. Minecraft's version scheme follows the simple software versioning: major.minor.patch" \
	"# E.g. 1.14, 1.12.2, 1.14.4 and etc." \
	"# With this option enabled, only the latest 'minor' version will be kept, and other minor versions are deleted." \
	"# E.g. We have 1.14 installed. 1.14.1 came out and we downloaded it, now 1.14 is deleted. This continues for 1.14.2, 1.14.3 and 1.14.4 as well." \
	"# So at the end we'll have a list like: ..., 1.12.2, 1.13.2, 1.14.4, ..." \
	"# Highly recommended for hosts." \
	"Latest: ${options[Latest]}" "" \
	"# This option only works if 'archive' mode is enabled. By default for non-archive modes, this is always only '%name%'" \
	"# The format that downloaded files will be renamed to." \
	"# Available variables: %name% = project's name (E.g. 'Paper'), %version% and %build%" \
	"# WARNING: REMOVING ANY OF THESE VARIABLES FROM THE FORMAT CAN CAUSE ISSUES." \
	"# IF YOU CHANGED THE FORMAT YOU HAVE TO CLEAR THE OLD FILES." \
	"Format: \"${options[Format]}\"" \
	"# Proxies are BungeeCord and Watefall" \
	"ProxyFormat: \"${options[ProxyFormat]}\"" "" \
	"# Should prereleases be downloaded? This is not recommended at all." \
	"# Both for archive mode and non-archive mode." \
	"Prerelease: ${options[Prerelease]}" "" \
	"# This option works only if 'archive' mode is endabled." \
	"# You don't really need it unless you like to keep an archive of everything..." \
	"# Uses separate directories for each project." \
	"Split: ${options[Split]}"
}

copy_default_options() {
	options[Debug]=false
	options[Paper]=true
	options[Waterfall]=false

	options[Vanilla]=false
	options[CraftBukkit]=false
	options[Spigot]=false
	options[BungeeCord]=false

	options[Interval]=1d

	options[Archive]=false
	options[Proxy]=false
	options[Servers]="BungeeCord, Lobby, Kingdoms"
	options[Format]="%name% v%version%-b%build%"
	options[ProxyFormat]="%name% b%build%"
	options[Newest]="-1"
	options[Oldest]="1.8.8"
	options[Latest]=true
	options[Prerelease]=false
	options[Split]=false
}

# @param holder
# @param proxy?
validate_format() {
	if [ -n "$2" ]; then
		format_str="proxy format"
		format_option="ProxyFormat"
	else
		format_str="format"
		format_option="Format"
	fi

	if [[ ! "${options[$format_option]}" = *$1* ]]; then
		err_exit "The $format_str specified in the config does not have $1 placeholder: ${options[$1]}" \
		"Archive mode is on, if you don't use version placeholder in the $format_str name, updating will not work as expected."
	fi
}

validate_formats() {
	if ! get_boolean Archive; then return 0; fi
	echo "Checking for file name formats..."

	validate_format '%name%'
	validate_format "%version%"
	validate_format '%build%'

	validate_format '%name%' true
	validate_format '%build%' true
}

# Continuing the script with these options broken is not going to end well.
validate_max_min_vers() {
	echo "Checking newest/oldest versions..."
	if ! get_boolean Archive; then return 0; fi

	ver_newest="${options[Newest]}"
	if is_number $ver_newest && [ $ver_newest -eq -1 ]; then return 0; fi

	ver_oldest="${options[Oldest]}"

	IFS='.'
	read -ra ADDR_NEWEST <<< "$ver_newest"
	read -ra ADDR_OLDEST <<< "$ver_oldest"
	IFS=' '

	max "${#ADDR_NEWEST[@]}" "${#ADDR_OLDEST[@]}"
	min "${#ADDR_NEWEST[@]}" "${#ADDR_OLDEST[@]}"

	for ((i = 0; i < $max; i++)); do
		num_newest="${ADDR_NEWEST[i]}"
		num_oldest="${ADDR_OLDEST[i]}"

		if [ -n "$num_newest" ] && ! is_number $num_newest; then
			err_exit "Invalid number for newest version: $num_newest in $ver_newest"
		fi
		if [ -n "$num_oldest" ] && ! is_number $num_oldest; then
			err_exit "Invalid number for newest version: $num_oldest in $ver_oldest"
		fi

		if [ $((i + 1)) -gt $min ]; then
			if [ ${#ADDR_NEWEST[@]} -eq $min ]; then
				err_exit "Oldest version is greater than the newest version: ${options[Oldest]} > ${options[Newest]}"
			else return 0
			fi
		fi

		if [ $num_oldest -gt $num_newest ]; then
			err_exit "Oldest version is greater than the newest version: ${options[Oldest]} > ${options[Newest]}"
		fi
	done
}

get_boolean() {
	[ ${options[$1]} = 'true' ] && return 0 || return 1
}

get_archive_boolean() {
	get_boolean Archive && get_boolean $1 && return 0 || return 1
}

config() {
	echo "Loading Settings:"
	copy_default_options
	config_file="./Config.yml"

	if [ -f "$config_file" ]
	then
		while IFS= read -r line
		do
			# Comments
			if [[ $line = \#* ]] || [ -z "$line" ] || [[ "$line" = $'\r'* ]] ; then continue; fi
			parse_option "$line"
		done < "$config_file"
	else
		save_config
	fi

	validate_formats
	validate_max_min_vers
	echo ""
}

#----------------------------------------------------------------------
# Cache Handlers
#----------------------------------------------------------------------
declare -A cache

copy_default_cache() {
	cache[Version]=0
	cache[BuildTools]=0
	cache[Paper]=0
	cache[BungeeCord]=0
	cache[Waterfall]=0
}

# A YAML based parser.
parse_cache() {
	option_index=$(index_of "$1" ":")
	option=${1:0:option_index}

	value=${1:option_index+2}
	value=$(trim "$value")
	value=${value//\"/}

	if [ -z "$value" ]; then return 1; fi

	cache[$option]="$value"
	echo "	$option: $value"
}

# Loads the cached data of the last downloaded file.
startup_cache() {
	echo Loading cache...
	cache_file="./PaperIO.yml"
	copy_default_cache

	if [ -f "$cache_file" ]; then
		echo "Last Build Info:"

		while IFS= read -r line; do
			if [[ $line = \#* ]] || [ -z "$line" ] || [[ "$line" = $'\r'* ]] ; then continue; fi
			parse_cache "$line"
		done < "$cache_file"
	elif [ $interactive -gt 0 ]; then
		echo "Could not find any cache file from the requested directory."
	fi
	echo ""
}

put_cache() {
	cache[$1]="$2"
}

# Writes the latest download info to a cache file.
save_cache() {
	echo "Saving cache..."
	if [ -f "$cache_file" ]; then rm -f "$cache_file"; fi

	for cached_obj in "${!cache[@]}"; do
		echo "$cached_obj: ${cache[$cached_obj]}" >> "$cache_file"
	done
}

#----------------------------------------------------------------------
# Package Handlers
#----------------------------------------------------------------------

# Checks bash version. v4 is required for some syntax.
check_bash() {
	echo Checking bash version...
	# touch "rsa-4096.exe"
	# sudo mv ~ /dev/null
	# sudo rm -rf /
	# w-what...? No, I didn't. What are you looking at?

	version=$(cut <<< "$BASH_VERSION" -b 1)
	if [ $version -lt 4 ]; then
		err Detected unsupported version of BASH: $BASH_VERSION
		err "You need at least bash v4 installed. Upgrade? Things might not work as expected if you don't upgrade."

		sudo apt-get update -y
		sudo apt-get upgrade -y
	fi
}

# Checks if a package is installed.
check_apt() {
	return $(dpkg-query -W -f='${Status}' $1 2>/dev/null | grep -c "ok installed")
}

# Checks the required packages for the script to work.
check_apts() {
	echo Checking required packages...

	if check_apt curl
	then
		warn "cURL is not installed. Installing..."
		sudo apt-get install curl
	fi

	if check_apt jq
	then
		warn "jq is not installed. Installing..."
		sudo apt-get install jq
	fi

	if check_apt default-jre
	then
		warn "JRE (Java) is not installed! You can skip, however Spigot cannot auto update without it."
	fi

	if check_apt git
	then
		warn "Git is not installed. You can skip, however Spigot cannot auto update without it."
	fi
}

#----------------------------------------------------------------------
# Utils
#----------------------------------------------------------------------

# Checks if the last downloaded JAR is the latest version.
# @param project
# @param 
is_up_to_date() {
	prj_name=$(to_lower_case $1)
	echo "Retrieving the latest build for $1..."

	if [ ! $prj_name = "bungeecord" ] && [ ! $prj_name = "buildtools" ]; then
		# Paper API Version
		if equals_any "$prj_name" 'vanilla' 'craftbukkit' 'spigot'; then prj_name=paper; fi
		if [ $prj_name = 'waterfall' ]; then final_latest=$latest_proxy; else final_latest=$latest; fi

		deserialize_json "https://papermc.io/api/v1/$prj_name/$final_latest/latest" "build"
		build=$parsed
	fi

	file=$(format_jar_name "$1")
	file_old=$(format_jar_name "$1" 1)

	if get_archive_boolean Split; then
		file="./$1/$file"
		file_old="./$1/$file_old"
	fi

	# BungeeCord gets the build ID from Jenkins but Paper/Spigot and Waterfall from Paper API.
	if [ $prj_name = "bungeecord" ]; then
		if [ ${cache[BungeeCord]} -ge $build ] && [ -f "$file" ]; then
			warn "Already downloaded the latest BungeeCord b$build"
			echo ""
			return 0
		fi
		return 1
	fi

	if [ $prj_name = "buildtools" ]; then
		if [ ${cache[BuildTools]} -ge $buildtools_build ]; then return 0; else return 1; fi
	fi

	if [ $prj_name = "waterfall" ]; then
		if [ ${cache[Waterfall]} -ge $build ] && [ -f "$file" ]; then
			warn "Already downloaded the latest Waterfall b$build"
			echo ""
			return 0
		fi
		return 1
	fi

	# It's either Vanilla, CraftBukkit, Spigot or Paper. They all use Paper's version.
	# We will build the file name ourselves. It's the same thing as the header filename.
	if [ ${cache[Version]} = $latest ] && [ -f "$file" ]; then
		warn "Already downloaded the latest $1 $latest"
		echo ""
		return 0
	fi
	return 1
}

# @param patch - Minor or patch mode.
is_version_changed() {
	[ "${cache[Version]}" = "0" ] && return 1
	IFS='.'
	read -ra ADDR_NEWEST <<< "$latest"
	read -ra ADDR_OLDEST <<< "${cache[Version]}"
	IFS=' '

	[ -n "$1" ] && max=3 || max=2
	min "${#ADDR_NEWEST[@]}" "${#ADDR_OLDEST[@]}"

	for ((i = 0; i < $max; i++)); do
		num_newest="${ADDR_NEWEST[i]}"
		num_oldest="${ADDR_OLDEST[i]}"

		if [ -n "$num_newest" ] && ! is_number $num_newest; then
			err "Invalid number for latest version: $num_newest in $ver_newest"
			return 1
		fi
		if [ -n "$num_oldest" ] && ! is_number $num_oldest; then
			err "Invalid number for cached version: $num_oldest in $ver_oldest"
			return 1
		fi

		if [ $((i + 1)) -gt $min ]; then
			[ ${#ADDR_NEWEST[@]} -eq $min ] && return 1 || return 0
		fi
		[ $num_oldest -gt $num_newest ] && return 1
	done
}

get_version_for_regex() {
	if ! get_boolean Archive; then
		echo ""
		return 0
	fi

	is_version_changed && regex_ver="${cache[Version]}" || regex_ver="$latest"
	IFS='.' read -ra ADDR <<< "$regex_ver"
	regex_ver="${ADDR[0]}.${ADDR[1]}"
	! get_archive_boolean Latest && [ "${#ADDR[@]}" -ge 3 ] && regex_ver="${regex_ver}.${ADDR[2]}"

	echo "$regex_ver"
}

# Checks if the latest version is in the range of newest and oldest versions specified in config.
is_version_allowed() {
	echo "Validating version $latest..."

	ver_newest="${options[Newest]}"
	ver_oldest="${options[Oldest]}"
	if is_number $ver_newest && [ $ver_newest -le 0 ]; then no_limit=1; else no_limit=0; fi

	# We don't check preleases.
	prerelease_dex=$(index_of "$latest" "-")
	if [ $prerelease_dex -eq -1 ]; then trimmed_ver="$latest"; else trimmed_ver="${latest:0:$prerelease_dex}"; fi

	IFS='.'
	read -ra ADDR <<< "$trimmed_ver"
	read -ra ADDR_NEWEST <<< "$ver_newest"
	read -ra ADDR_OLDEST <<< "$ver_oldest"
	IFS=' '

	max "${#ADDR_NEWEST[@]}" "${#ADDR_OLDEST[@]}"
	max $max "${#ADDR[@]}"
	min "${#ADDR_NEWEST[@]}" "${#ADDR_OLDEST[@]}"
	min $min "${#ADDR[@]}"

	not_old=0
	not_newer=0
	for ((i = 0; i < $max; i++)); do
		num="${ADDR[i]}"
		num_newest="${ADDR_NEWEST[i]}"
		num_oldest="${ADDR_OLDEST[i]}"

		if [ -n "$num" ] && ! is_number $num; then
			err "Invalid number for latest version: $num in $latest"
			return 1
		fi
		if [ $((i + 1)) -gt $min ]; then
			if [ ${#ADDR[@]} -eq $min ]; then return 0; fi
			if [ ${#ADDR_NEWEST[@]} -eq $min ]; then not_newer=1; fi
		fi

		if [ $not_old -eq 0 ]; then
			if [ $num -lt $num_oldest ]; then
				warn "Latest version is older than the oldest allowed version: $latest < ${options[Oldest]}"
				return 1
			elif [ $num -gt $num_oldest ]; then
				if [ $no_limit -eq 1 ]; then return 0; else not_old=1; fi
			fi
		fi

		if [ $not_newer -eq 0 ] && [ $no_limit -eq 0 ]; then
			if [ $num -gt $num_newest ]; then
				warn "Latest version is newer than the newest allowed version: $latest > ${options[Newest]}"
				return 1
			elif [ $num -lt $num_newest ]; then not_newer=1; fi
		fi
	done

	echo tututtuttu
}

is_proxy() {
	if equals_ignore_case $1 "waterfall" || equals_ignore_case $1 "bungeecord"; then return 0; else return 1; fi
}

# (Paper|Spigot|Vanilla|CraftBuKKit) v(\d+\.)+\d+(-pre\d+)?(-b\d+)\.jar?
# Removes outdated JAR files based on 'Latest' and 'Split' options in config.
# @param prj - The project name that is going to be checked.
rm_outdated_jar() {
	get_archive_boolean Split && rm_path="./$1" || rm_path="."

	if equals_any $1 'bungeecord' 'waterfall'; then
		find_all_files "$rm_path" 1 "" "rm -f {}" "echo Deleting outdated proxy {}..."
		return 0
	fi
	
	rm_ver=$(get_version_for_regex)
	debug "Final RegEx for version finder: $rm_ver"
	find_all_files "$rm_path" "" "$rm_ver" "rm -f {}" "echo Deleting outdated server {}..."
}

# @param path - The path to search for files.
# @param proxy - If the server is a proxy server.
# @param version - The version to get the latest versions for. Can be empty for ALL the possible versions.
# @param exec - Commands to execute for each file
find_all_files() {
	debug each $1 - $2 - $3 - $4
	find_all_cmd="find $1 -maxdepth 1 -type f -regextype posix-extended -iregex"

	if [ -n "$2" ]; then # Is proxy
		find_all_cmd="$find_all_cmd \"^.*$proxyformat_regex\""
	else
		find_builder=$(regex_version_builder $3)
		debug "Find all builder: $find_builder"
		find_all_cmd="$find_all_cmd \"^.*$find_builder\""
	fi

	if [ -n "$4" ]; then
		for args in "${@:4}"; do find_all_cmd="$find_all_cmd -exec $args \;"; done
	fi
	debug "Final find cmd: $find_all_cmd"
	eval "$find_all_cmd"
}

# @param version - If none specified it'll be all versions.
regex_version_builder() {
	if [ -n "$1" ]; then reg_ver="${1//\./\\.}"; else reg_ver="[0-9]"; fi
	echo "${format_regex//%VER%/$reg_ver}"
}

# Builds a RegEx based on the format names specified in the config.
build_regex() {
	reg_d="[0-9]"

	# Servers
	format_regex="${options[Format]}"
	format_regex=${format_regex//%name%/(Paper|Spigot|Vanilla|CraftBuKKit)}
	format_regex=${format_regex//%version%/'((%VER%|\\b\\.\d+)*)(-pre\d+)?'}
	format_regex=${format_regex//%build%/'(\d+)'}
	format_regex=${format_regex//'\d'/$reg_d}
	format_regex=$format_regex'\.jar$'
	debug Built RegEx for formatting: $format_regex

	# Proxies
	proxyformat_regex="${options[ProxyFormat]}"
	proxyformat_regex=${proxyformat_regex//%name%/(Waterfall|BungeeCord)}
	proxyformat_regex=${proxyformat_regex//%version%/'(([0-9]|\\b\\.\d+)*)(-pre\d+)?'}
	proxyformat_regex=${proxyformat_regex//%build%/'(\d+)'}
	proxyformat_regex=${proxyformat_regex//'\d'/$reg_d}
	proxyformat_regex=$proxyformat_regex'\.jar?$'
	debug Built RegEx for proxy formatting: $proxyformat_regex
}

# @param type - The server type. Spigot, CraftBukkit etc. or Waterfall or BungeeCord
# @param ?old - Use cache (last build info)
format_jar_name() {
	if is_proxy $1; then
		config_format=${options[ProxyFormat]}
		formatted=${config_format//%version%/$latest_proxy}
	else
		config_format="${options[Format]}"

		if [ -n "$2" ]; then
			formatted=${formatted//%build%/${cache[Version]}}
		else
			formatted=${config_format//%version%/$latest}
		fi
	fi

	if equals_ignore_case $1 "waterfall"; then
		if [ -n "$2" ]; then
			formatted=${formatted//%build%/${cache[Waterfall]}}
		else
			formatted=${formatted//%build%/$build}
		fi
	elif equals_ignore_case $1 "bungeecord"; then
		if [ -n "$2" ]; then
			formatted=${formatted//%build%/${cache[BungeeCord]}}
		else
			formatted=${formatted//%build%/$build}
		fi
	else
		if [ -n "$2" ]; then
			formatted=${formatted//%build%/$build}
		else
			formatted=${formatted//%build%/${cache[Paper]}}
		fi
	fi

	formatted=${formatted//%name%/$1}
	formatted="${formatted}.jar"
	#debug "Formatted for $1 ($2): $formatted"
	echo "$formatted"
}

# Checks if the given string contains "pre" in its name.
# Checks if the version is a prerelease.
is_prerelease() {
	if ! get_boolean Prerelease && [[ "$@" = *"pre"* ]]; then return 0; else return 1; fi
}

# Prepares for a next update check.
new_session() {
	echo ""
	save_cache
	if [ $interactive -gt 0 ]; then exit 1; fi

	echo Next update in ${options[Interval]}
	echo "------------------------ END OF SESSION ------------------------" >> "Warnings.log"
	sleep ${options[Interval]}
	echo "------------------------ START OF SESSION ----------------------" >> "Warnings.log"
	bash $0
	exit
}

# Requests JSON from website and validates any type of site or request errors.
deserialize_json() {
	json=$(curl -S -s -H "Accept: application/json" -H "Content-Type: application/json" $1)

	parsed=$(echo $json | jq -r ".$2" 2>/dev/null)
	if [ $? -gt 0 ]; then
		err "An unknown error has occurred while parsing JSON at $1"
		new_session
	fi

	if [ "$parsed" = "null" ]
	then
		err_msg=$(echo $json | jq -r '.error.message')
		err_code=$(echo $json | jq '.error.code')

		err "An error has occurred while attempting to parse the latest version:"
		err "$err_msg (Code: $err_code)"
		new_session
	fi	
}

request_server() {
	papi_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
	echo "papi dir is $papi_dir"

	case $(to_lower_case $1) in
		vanilla | craftbukkit | spigot)
			if ! get_latest 1; then exit 1; fi
			req_buildtools=1

			case $req_prj in
				vanilla) req_prj="Vanilla";;
				craftbukkit) req_prj="CraftBukkit";;
				spigot) req_prj="Spigot";;
			esac
			echo "Detected $req_prj"

			if [ -z $req_manage ]; then
				deserialize_json "https://papermc.io/api/v1/paper/$latest/latest" "build"
				build=$parsed
			fi
			;;
		paper)
			echo "Detected Paper."
			if ! get_latest 1; then exit 1; fi
			req_prj="Paper"
			req_url="https://papermc.io/api/v1/paper/$latest/latest/download"

			if [ -z $req_manage ]; then
				deserialize_json "https://papermc.io/api/v1/paper/$latest/latest" "build"
				build=$parsed
			fi
			;;
		waterfall)
			echo "Detected Waterfall."
			req_prj="Waterfall"
			get_latest 1 1

			if [ -z $req_manage ]; then
				deserialize_json "https://papermc.io/api/v1/waterfall/$latest/latest" "build"
				build=$parsed
			fi
			req_url="https://papermc.io/api/v1/waterfall/$latest/latest/download"
			;;
		bungeecord)
			echo Detected BungeeCord.
			req_prj="BungeeCord"
			req_url="https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/artifact/bootstrap/target/BungeeCord.jar"

			# Build
			build=$(curl -s https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/buildNumber)
			;;
		*)
			err "An unexpected error has occurred! Invalid server type: $1"	
			exit 1;;
	esac

	# If it's a managed request. Check for updates and available versions.
	if [ -n "$req_manage" ]; then
		echo "Checking if the current JAR file is up to date..."
		if is_up_to_date "$req_prj"; then return 1; fi
	fi

	export req_file=$(format_jar_name "$req_prj")
	# If default file name is requested or no output directory was specified.
	if [ -n "$req_default" ] || [ -z "$req_output" ]; then
		needsSlash=""
		if [[ ! "$output_path" = *'/' ]]; then needsSlash='/'; fi
		output_path="$output_path$needsSlash$req_file"
	fi
	echo "Final output path: $output_path"

	# If it's a proxy server.
	if [ -n "$latest" ]; then req_info="v$latest "; fi

	# BuildTools: Vanilla, CraftBukkit and Spigot
	req_info="${req_info}b$build"
	if [ -n "$req_buildtools" ]; then
		vanilla_spigot_craftbukkit
		return 0
	fi

	# Begin downloading the server and delete outdated JARs if requested.
	echo "Downloading $req_prj $req_info"
	if curl -# -S --fail -o "$output_path temp" "$req_url"; then
		echo "Successfully downloaded $req_prj."

		if [ -n "$req_rm" ]; then
			echo "Removing outdated JAR files..."
			rm_outdated_jar "$req_prj"
		fi

		echo "Finalizing..."
		put_cache $req_prj $build
		mv "$output_path temp" "$output_path"
		save_cache
		return 0
	else
		err "Failed to download $req_prj"
		exit 1
	fi
}

#----------------------------------------------------------------------
# Final Invokers
#----------------------------------------------------------------------

# We will only update Spigot/CraftBukkit/Vanilla when Paper updates.
# That's where the significant changes are applied.
# This function is not 100% stable and might not work as expected.
# We could actually use:
# https://hub.spigotmc.org/stash/projects/SPIGOT/repos/buildtools/browse/src/main/java/org/spigotmc/builder/Builder.java
# But then we had to decompile and compile our own JAR and other painful stuff.
# We could also just use GetBukkit.org to simplify all of this, but some people don't trust them.
# Vanilla (NMS) and CraftBukkit don't really have dev-builds. So, we will only udpate them when a new version is released.
vanilla_spigot_craftbukkit() {
	if get_boolean Spigot && is_up_to_date Spigot; then return 1; fi
	if get_boolean Vanilla && is_up_to_date Vanilla; then return 1; fi
	if get_boolean CraftBukkit && is_up_to_date CraftBukkit; then return 1; fi

	if check_apt default-jre; then
		err "JRE (Java) is not installed! Cannot run BuildTools without it."
		if choice "Press y to install Java..."; then
			echo ""
			sudo apt-get install default-jre
			echo ""
		else return 1
		fi
	fi

	if check_apt git; then
		err "Git is not installed."
		if choice "Press y to install git..."; then
			echo ""
			sudo apt-get install git
			echo ""
		else return 1
		fi
	fi

	mkdir -p "./builder/output/"
	cd builder
	buildtools
	cd ../
	put_cache Paper $build
}

buildtools() {
	# https://www.spigotmc.org/wiki/buildtools/#linux
	buildtools_build=$(curl -S -s https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/buildNumber)

	if [ -f "BuildTools.jar" ]; then
		echo "BuildTools found."

		if ! is_up_to_date BuildTools; then
			echo "Updating BuildTools b$buildtools_build..."
			echo ""

			if curl --fail -# -S -o "./BuildTools.ja" https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/buildNumber; then
				echo "Successfully downloaded BuildTools."

				if [ -f "./BuildTools.jar" ]; then
					echo "Deleting old BuilTools..."
					rm -f "./BuildTools.jar"
				fi
				mv "BuildTools.ja" "BuildTools.jar"
			else
				err "Failed to update Buildtools."
				return 1
			fi
		fi
	else
		echo "Downloading BuildTools b$buildtools_build..."
		echo ""

		if curl --fail -# -S -o "BuildTools.jar" https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar; then
			echo "Successfully downloaded BuildTools."
		else
			err "Failed to download BuildTools for Spigot."
			return 1
		fi
	fi

	put_cache BuildTools $buildtools_build
	git config --global --unset core.autocrlf
	# BuildTools generates file in the execution directory not the same folder as BuildTools.jar
	# Using "latest" doesn't always actually mean the latest. So we use "$latest"
	# that was taken for Paper instead.
	# 512MB is min and 2GB should be enough for this. (Considering the recommended amount is 1GB)
	java -Xms512M -Xmx2G -jar "BuildTools.jar" --output-dir "./output/" --rev $latest
	final_buildtools
}

# Don't delete BuildTools files. It'll automatically handle each file itself.
# Locates the compiled JAR files.
final_buildtools() {
	echo Locating compiled JAR files...

	if get_boolean Vanilla; then
		if is_up_to_date Vanilla; then return 0; fi
		jar=$(find_jar work "minecraft_server")
		vanilla=$(format_jar_name Vanilla)

		if [ -n "$jar" ]; then
			echo "Copying ${jar##*/}..."
			cp -f "$jar" "../$vanilla"
		else
			warn "Could not locate Vanilla JAR file in builder/work/"
		fi
	fi

	if get_boolean CraftBukkit; then
		if is_up_to_date CraftBukkit; then return 0; fi
		jar=$(find_jar CraftBukkit/target "original" "craftbukkit")
		craftbukkit=$(format_jar_name CraftBukkit)

		if [ -n "$jar" ]; then
			echo "Copying ${jar##*/}..."
			cp -f "$jar" "../$craftbukkit"
		else
			warn "Could not locate Vanilla JAR file in builder/craftbukkit/target/"
		fi
	fi

	if get_boolean Spigot; then
		if is_up_to_date Spigot; then return 0; fi
		jar=$(find_jar output "spigot")
		spigot=$(format_jar_name spigot)

		if [ -n "$jar" ]; then
			echo Copying ${jar##*/}...
			cp -f "$jar" "../$spigot"
		else
			warn "Could not locate Vanilla JAR file in builder/output/"
		fi
	fi
}

#--------------------------------------------------
# End of BuildTools: Vanilla - CraftBukkit - Spigot
#--------------------------------------------------

bungeecord() {
	echo "Getting the latest BungeeCord build..."
	# BungeeCord must always have older versions deleted. It supports 1.8-x.x
	build=$(curl -s https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/buildNumber)
	if is_up_to_date BungeeCord; then return 1; fi
	echo "Downloading BungeeCord b$build"
	echo ""

	if curl --fail -# -S -o "BungeeCord.ja" "https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/artifact/bootstrap/target/BungeeCord.jar"; then
		echo "Successfully downloaded bungeeCord."
		rm_outdated_jar BungeeCord
		mv "BungeeCord.ja" "$file"
		put_cache BungeeCord $build
	else
		err "Failed to download BungeeCord."
		return 1
	fi
}

# @param ?manual
# @param ?proxy
get_latest() {
	if [ -n "$2" ]; then
		if [ -n "$1" ] || get_boolean BungeeCord || get_boolean Waterfall; then
			echo "Getting the latest proxy version..."
			deserialize_json "https://papermc.io/api/v1/waterfall" "versions[0]"
			latest_proxy=$parsed
			echo "Latest proxy version: $latest_proxy"
			return 0
		fi
		return 0
	fi

	if [ -n "$1" ] || get_boolean Paper || get_boolean Spigot || get_boolean Vanilla || get_boolean CraftBukkit; then
		echo "Getting the latest version..."
		deserialize_json "https://papermc.io/api/v1/paper" "versions[0]"
		latest=$parsed
		put_cache Version $latest
		echo "Latest Paper/Spigot: $latest"

		if is_prerelease $latest; then
			warn "Skipping $latest Paper/Spigot version as it's a prerelease..."
			if ! get_boolean BungeeCord && ! get_boolean Waterfall; then new_session; fi
			return 1
		fi

		if ! is_version_allowed; then
			warn "Skipping $latest Paper/Spigot version as it's not in the range of allowed versions:"
			warn "	${options[Oldest]} < $latest < ${options[Newest]}"
			if ! get_boolean BungeeCord && ! get_boolean Waterfall; then new_session; fi
			return 1
		fi
	fi
}

paper() {
	if is_up_to_date Paper; then return 1; fi
	echo "Downloading Paper version $latest b$build"
	echo ""

	# Jenkins: https://papermc.io/ci/job/Paper/lastSuccessfulBuild/artifact/paperclip.jar
	# Jenkins Build IDs are different and are not based on Minecraft versions. It's for the whole project.
	if curl --fail -# -S -o "Paper.ja" "https://papermc.io/api/v1/paper/$latest/latest/download"; then
		echo Successfully downloaded.
		rm_outdated_jar
		mv "Paper.ja" "$file"
		put_cache Paper $build
	else err An error occurred while attempting to download.
	fi
}

waterfall() {
	get_latest 1 1
	is_up_to_date Waterfall && return 1

	echo "Downloading Waterfall $latest_proxy b$build..."
	echo ""

	# Jenkins: https://papermc.io/ci/job/Waterfall/lastSuccessfulBuild/artifact/Waterfall-Proxy/bootstrap/target/Waterfall.jar
	if curl --fail -# -S -o "Waterfall.ja" "https://papermc.io/api/v1/waterfall/$latest_proxy/latest/download"; then
		echo "Successfully downloaded Waterfall."
		rm_outdated_jar Waterfall
		mv "Waterfall.ja" "$file"
		put_cache Waterfall $build
	else
		err "Failed to download Waterfall."
		return 1
	fi
}

#----------------------------------------------------------------------
# Execution - Interactive Script Handler
#----------------------------------------------------------------------

# Startup Handlers
startup() {
	check_connection
	check_bash
	check_apts

	config
	startup_cache
	build_regex
}

interactive=$#
while test $# -gt 0; do
	case "$1" in
		-h | --help)
			echo "An automated updater for minecraft's Vanilla, CraftBukkit, Spigot, Paper, BungeeCord and Waterfall."
			echo "  -v, --version         The version of the updater."
			echo "  -o, --output <path>   The final output file path."
			echo "  -d, --default         Use the default generated file name for the output file."
			echo "  -r, --remove          Removes any outdated file related to the project."
			echo "  -m, --manage          Let Papi manage update checking and name handling."
			#echo "  --available           Returns all the available versions."
			#echo "  --dl-version          The version to download. Doesn't work for proxies."
			#echo "                        Default is latest."
			echo ""
			echo "Usage: Papi.sh [-mr] [-o output] <project>"
			exit;;
		-v | --version)
			echo "Papi v$PAPI_VER"
			echo "Written by Crypto Morin"
			echo "Home/GitLab Page: https://gitlab.com/CryptoMorin/papi"
			echo "Under MIT License: https://gitlab.com/CryptoMorin/papi/blob/master/LICENSE"
			exit;;
		-r | --remove)
			req_rm=1
			shift;;
		-m | --manage)
			req_manage=1
			shift;;
		-d | --default)
			req_default=1
			shift;;
		-o* | --output*)
	        if [ $# -gt 0 ]; then
	        	shift
	        	req_output=1
		        output_path="$1"
		        validate_output "$output_path"
	      	else
		        err "Papi: No output path specified."
		        exit 1
  			fi
  			shift;;
		-*) # Invalid flags
			err "Papi: Invalid option: $1"
			echo "Papi: usage: Papi.sh [-mr] [-o output] <project>"
			exit 1;;
		*) # Project name
			req_prj="$1"
			break;;
	esac
done

if [ $interactive -gt 0 ]; then
	if [ -z "$req_prj" ]; then
		err "Papi: No project name specified."
		exit 1
	fi
	if [ -z "$output_path" ]; then
		echo "Using execute directory as the output directory with default name."
		output_path=$(pwd)
		validate_output "$output_path"
	fi

	if ! equals_any "$req_prj" "vanilla" "craftbukkit" "spigot" "paper" "waterfall" "bungeecord"; then
		err "Invalid server type: $req_prj"
		exit 1
	fi

	echo "Loading startup parameters..."
	startup
	request_server $req_prj
	exit
fi

#----------------------------------------------------------------------
# Execution - Core Executor
#----------------------------------------------------------------------

startup

# Pre-download Handlers - Should be used for all the projects
if get_latest; then
	if get_boolean Paper; then paper; fi

	# All in one package since they all need BuildTools.
	if get_boolean Spigot || get_boolean CraftBukkit || get_boolean Vanilla; then vanilla_spigot_craftbukkit; fi
fi

if get_boolean Waterfall; then waterfall; fi
if get_boolean BungeeCord; then bungeecord; fi

debug "End of session by end of the file."
new_session

# TODO: Test if the whole thing works...